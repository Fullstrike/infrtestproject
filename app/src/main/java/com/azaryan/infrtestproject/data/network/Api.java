package com.azaryan.infrtestproject.data.network;

import com.azaryan.infrtestproject.models.AnimalResponse;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface Api {
    @GET("api.php")
    Single<AnimalResponse> getAnimals(@Query("query") String animalType);
}
