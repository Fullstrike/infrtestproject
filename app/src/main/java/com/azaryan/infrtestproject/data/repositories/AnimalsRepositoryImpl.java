package com.azaryan.infrtestproject.data.repositories;

import com.azaryan.infrtestproject.data.network.Api;
import com.azaryan.infrtestproject.models.AnimalResponse;

import io.reactivex.Single;

public class AnimalsRepositoryImpl implements AnimalsRepository {
    private Api api;

    public AnimalsRepositoryImpl(Api api) {
        this.api = api;
    }

    @Override
    public Single<AnimalResponse> getAnimals(String animalType) {
        return api.getAnimals(animalType);
    }
}
