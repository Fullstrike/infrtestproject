package com.azaryan.infrtestproject.data.repositories;

import com.azaryan.infrtestproject.models.AnimalResponse;

import io.reactivex.Single;

public interface AnimalsRepository {
    Single<AnimalResponse> getAnimals(String animalType);
}
