package com.azaryan.infrtestproject;

import com.azaryan.infrtestproject.data.network.Api;
import com.azaryan.infrtestproject.data.repositories.AnimalsRepository;
import com.azaryan.infrtestproject.data.repositories.AnimalsRepositoryImpl;
import com.azaryan.infrtestproject.domain.AnimalsInteractor;
import com.azaryan.infrtestproject.domain.AnimalsInteractorImpl;
import com.azaryan.infrtestproject.domain.AnimalsResponseMapper;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

// Maybe for example this will be enough
public class DependencyProvider {
    private static final String BASE_URL = "http://kot3.com/xim/api.php/";
    private static DependencyProvider provider;

    private Api api;
    private AnimalsRepository repository;
    private AnimalsInteractor interactor;

    public static DependencyProvider getInstance() {
        if (provider == null) {
            provider = new DependencyProvider();
        }

        return provider;
    }

    public AnimalsInteractor provideInteractor() {
        if (interactor != null) {
            return interactor;
        }

        interactor = new AnimalsInteractorImpl(provideRepository(), new AnimalsResponseMapper());

        return interactor;
    }

    private AnimalsRepository provideRepository() {
        if (repository != null) {
            return repository;
        }

        repository = new AnimalsRepositoryImpl(provideApi());

        return repository;
    }

    private Api provideApi() {
        if (api != null) {
            return api;
        }

        api = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(new OkHttpClient())
                .baseUrl(BASE_URL)
                .build()
                .create(Api.class);

        return api;
    }
}
