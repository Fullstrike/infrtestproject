package com.azaryan.infrtestproject.domain;

import com.azaryan.infrtestproject.data.repositories.AnimalsRepository;
import com.azaryan.infrtestproject.models.AnimalViewModel;

import java.util.List;

import io.reactivex.Single;

public class AnimalsInteractorImpl implements AnimalsInteractor {
    private static final String CAT_TYPE = "cat";
    private static final String DOG_TYPE = "dog";

    private final AnimalsRepository repository;
    private final AnimalsResponseMapper mapper;

    public AnimalsInteractorImpl(AnimalsRepository repository, AnimalsResponseMapper mapper) {
        this.repository = repository;
        this.mapper = mapper;
    }

    @Override
    public Single<List<AnimalViewModel>> getCats() {
        return repository.getAnimals(CAT_TYPE)
                .map(mapper);
    }

    @Override
    public Single<List<AnimalViewModel>> getDogs() {
        return repository.getAnimals(DOG_TYPE)
                .map(mapper);
    }
}
