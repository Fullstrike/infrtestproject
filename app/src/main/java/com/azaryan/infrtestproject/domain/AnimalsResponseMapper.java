package com.azaryan.infrtestproject.domain;

import com.azaryan.infrtestproject.models.Animal;
import com.azaryan.infrtestproject.models.AnimalResponse;
import com.azaryan.infrtestproject.models.AnimalViewModel;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.functions.Function;

public class AnimalsResponseMapper implements Function<AnimalResponse, List<AnimalViewModel>> {
    @Override
    public List<AnimalViewModel> apply(AnimalResponse animalResponse) throws Exception {
        List<AnimalViewModel> result = new ArrayList<>();
        List<Animal> animals = animalResponse.getAnimals();
        if (animals == null || animals.isEmpty()) {
            return result;
        }

        for (Animal animal : animals) {
            AnimalViewModel viewModel = new AnimalViewModel(
                    animal.getTitle(),
                    "Description",
                    animal.getUrl()
            );
            result.add(viewModel);
        }

        return result;
    }
}
