package com.azaryan.infrtestproject.domain;

import com.azaryan.infrtestproject.models.AnimalViewModel;

import java.util.List;

import io.reactivex.Single;

public interface AnimalsInteractor {
    Single<List<AnimalViewModel>> getCats();

    Single<List<AnimalViewModel>> getDogs();
}
