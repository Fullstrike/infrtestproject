package com.azaryan.infrtestproject;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.azaryan.infrtestproject.presentation.view.cats.CatsFragment;
import com.azaryan.infrtestproject.presentation.view.dogs.DogsFragment;

public class MainActivity extends AppCompatActivity {
    private static final String BUNDLE_TAB_POSITION = "BUNDLE_TAB_POSITION";

    private TabLayout.BaseOnTabSelectedListener listener = new TabLayout.BaseOnTabSelectedListener() {
        @Override
        public void onTabSelected(TabLayout.Tab tab) {
            int position = tab.getPosition();
            String fragmentTag = getFragmentTagByPosition(position);
            Fragment fragment = getSupportFragmentManager().findFragmentByTag(fragmentTag);
            if (fragment != null) {
                getSupportFragmentManager().beginTransaction()
                        .show(fragment)
                        .commit();
                return;
            }

            addFragmentDependingOnTag(fragmentTag);
        }

        @Override
        public void onTabUnselected(TabLayout.Tab tab) {
            int position = tab.getPosition();
            String fragmentTag = getFragmentTagByPosition(position);
            Fragment fragment = getSupportFragmentManager().findFragmentByTag(fragmentTag);
            if (fragment != null) {
                getSupportFragmentManager().beginTransaction()
                        .hide(fragment)
                        .commit();
            }
        }

        @Override
        public void onTabReselected(TabLayout.Tab tab) {

        }

        private String getFragmentTagByPosition(int position) {
            switch (position) {
                case 0:
                    return CatsFragment.TAG;
                case 1:
                    return DogsFragment.TAG;
                default:
                    return CatsFragment.TAG;
            }
        }
    };

    private TabLayout tabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tabLayout = findViewById(R.id.tab_layout);

        if (savedInstanceState != null) {
            int tabPosition = savedInstanceState.getInt(BUNDLE_TAB_POSITION, 0);
            TabLayout.Tab tabAt = tabLayout.getTabAt(tabPosition);
            if (tabAt != null) {
                tabAt.select();
            }
        } else {
            if (getSupportFragmentManager().getFragments().isEmpty()) {
                addFragmentDependingOnTag(CatsFragment.TAG);
            }
        }

        tabLayout.addOnTabSelectedListener(listener);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(BUNDLE_TAB_POSITION, tabLayout.getSelectedTabPosition());
    }

    private void addFragmentDependingOnTag(String tag) {
        if (tag.equals(DogsFragment.TAG)) {
            addFragment(DogsFragment.newInstance(), DogsFragment.TAG);
        } else if (tag.equals(CatsFragment.TAG)) {
            addFragment(CatsFragment.newInstance(), CatsFragment.TAG);
        }
    }

    private void addFragment(Fragment fragment, String tag) {
        getSupportFragmentManager().beginTransaction()
                .add(R.id.fragment_container, fragment, tag)
                .commit();
    }
}
