package com.azaryan.infrtestproject.presentation.presenter.base;

import android.support.annotation.Nullable;

import com.azaryan.infrtestproject.presentation.view.base.BaseView;

public abstract class BasePresenter<T extends BaseView> {
    private T view;
    private boolean isFirstAttach = true;

    public void onAttach(T view) {
        if (isFirstAttach) {
            isFirstAttach = false;
            onFirstViewAttach(view);
        }

        this.view = view;
    }

    protected void onFirstViewAttach(T view) {

    }

    public void onDetach(T view) {
        this.view = null;
    }

    public void onDestroy(T view) {
        this.view = null;
    }

    @Nullable
    protected T getView() {
        return view;
    }
}
