package com.azaryan.infrtestproject.presentation.view.list;

import com.azaryan.infrtestproject.models.AnimalViewModel;
import com.azaryan.infrtestproject.models.ListViewState;
import com.azaryan.infrtestproject.presentation.view.base.BaseView;

public interface ListView extends BaseView {
    void updateViewState(ListViewState viewState);

    void openDetailScreen(AnimalViewModel viewModel);
}
