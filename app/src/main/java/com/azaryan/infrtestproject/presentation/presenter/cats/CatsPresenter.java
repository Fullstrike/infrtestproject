package com.azaryan.infrtestproject.presentation.presenter.cats;

import com.azaryan.infrtestproject.domain.AnimalsInteractor;
import com.azaryan.infrtestproject.models.AnimalViewModel;
import com.azaryan.infrtestproject.presentation.presenter.list.ListPresenter;
import com.azaryan.infrtestproject.presentation.view.cats.CatsView;

import java.util.List;

import io.reactivex.Single;

public class CatsPresenter extends ListPresenter<CatsView> {
    private final AnimalsInteractor interactor;

    public CatsPresenter(AnimalsInteractor interactor) {
        this.interactor = interactor;
    }

    protected Single<List<AnimalViewModel>> getRequest() {
        return interactor.getCats();
    }
}
