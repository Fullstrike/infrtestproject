package com.azaryan.infrtestproject.presentation.view.base;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.azaryan.infrtestproject.presentation.mvp.PresenterManager;
import com.azaryan.infrtestproject.presentation.presenter.base.BasePresenter;

public abstract class BaseFragment<P extends BasePresenter> extends Fragment implements BaseView {
    private PresenterManager manager;
    private boolean isStateSaved;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        manager = PresenterManager.getInstance();
        BasePresenter presenter = getPresenter();
        if (presenter == null) {
            presenter = createPresenter();
            manager.putPresenter(getPresenterTag(), presenter);
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        isStateSaved = true;
    }

    @Override
    public void onStart() {
        super.onStart();
        isStateSaved = false;
        getPresenter().onAttach(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        isStateSaved = false;
    }

    @Override
    public void onStop() {
        super.onStop();
        getPresenter().onDetach(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (requireActivity().isFinishing()) {
            destroyPresenter();
        }

        if (isStateSaved) {
            isStateSaved = false;
            return;
        }

        if (isRemoving()) {
            destroyPresenter();
        }
    }

    private void destroyPresenter() {
        getPresenter().onDestroy(this);
        manager.removePresenter(getPresenterTag());
    }

    protected P getPresenter() {
        return (P) manager.getPresenter(getPresenterTag());
    }

    protected abstract BasePresenter createPresenter();

    private String getPresenterTag() {
        return getClass().getCanonicalName();
    }
}
