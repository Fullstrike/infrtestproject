package com.azaryan.infrtestproject.presentation.view.list;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.azaryan.infrtestproject.R;
import com.azaryan.infrtestproject.models.AnimalViewModel;
import com.azaryan.infrtestproject.models.ListViewState;
import com.azaryan.infrtestproject.presentation.presenter.list.ListPresenter;
import com.azaryan.infrtestproject.presentation.view.base.BaseFragment;
import com.azaryan.infrtestproject.presentation.view.detail.DetailsAnimalActivity;


public abstract class ListFragment<P extends ListPresenter> extends BaseFragment<P> implements ListView {

    private ProgressBar progressBar;
    private AnimalsAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_list, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        progressBar = view.findViewById(R.id.progressBar);
        RecyclerView recyclerView = view.findViewById(R.id.recycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new AnimalsAdapter(viewModel -> getPresenter().onItemClicked(viewModel));
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void updateViewState(ListViewState viewState) {
        progressBar.setVisibility(viewState.isProgressVisible() ? View.VISIBLE : View.GONE);
        adapter.setItems(viewState.getItems());
    }

    @Override
    public void openDetailScreen(AnimalViewModel viewModel) {
        DetailsAnimalActivity.start(getContext(), viewModel);
    }
}
