package com.azaryan.infrtestproject.presentation.presenter.list;

import android.util.Log;

import com.azaryan.infrtestproject.models.AnimalViewModel;
import com.azaryan.infrtestproject.models.ListViewState;
import com.azaryan.infrtestproject.presentation.presenter.base.BasePresenter;
import com.azaryan.infrtestproject.presentation.view.list.ListView;

import java.util.List;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public abstract class ListPresenter<T extends ListView> extends BasePresenter<T> {
    private ListViewState viewState = new ListViewState();
    private Disposable requestDisposable;

    @Override
    protected void onFirstViewAttach(T view) {
        super.onFirstViewAttach(view);
        requestDisposable = getRequest()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(any -> showProgress())
                .doFinally(this::hideProgress)
                .subscribe(this::showItems, error -> Log.e("Api", error.getMessage()));
    }

    @Override
    public void onAttach(T view) {
        super.onAttach(view);
        updateViewState();
    }

    @Override
    public void onDestroy(T view) {
        super.onDestroy(view);
        if (requestDisposable != null) {
            requestDisposable.dispose();
        }
    }

    protected abstract Single<List<AnimalViewModel>> getRequest();

    private void showProgress() {
        viewState.showProgress();
        updateViewState();
    }

    private void hideProgress() {
        viewState.hideProgress();
        updateViewState();
    }

    private void showItems(List<AnimalViewModel> viewModels) {
        viewState.showItems(viewModels);
        updateViewState();
    }

    private void updateViewState() {
        T view = getView();
        if (view != null) {
            view.updateViewState(viewState);
        }
    }

    public void onItemClicked(AnimalViewModel viewModel) {
        T view = getView();
        if (view != null) {
            view.openDetailScreen(viewModel);
        }
    }
}
