package com.azaryan.infrtestproject.presentation.view.cats;

import com.azaryan.infrtestproject.DependencyProvider;
import com.azaryan.infrtestproject.presentation.presenter.base.BasePresenter;
import com.azaryan.infrtestproject.presentation.presenter.cats.CatsPresenter;
import com.azaryan.infrtestproject.presentation.view.list.ListFragment;

public class CatsFragment extends ListFragment<CatsPresenter> implements CatsView {
    public static final String TAG = "CatsFragment";

    public static CatsFragment newInstance() {
        return new CatsFragment();
    }

    @Override
    protected BasePresenter createPresenter() {
        return new CatsPresenter(DependencyProvider.getInstance().provideInteractor());
    }
}
