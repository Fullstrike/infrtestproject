package com.azaryan.infrtestproject.presentation.presenter.dogs;

import com.azaryan.infrtestproject.domain.AnimalsInteractor;
import com.azaryan.infrtestproject.models.AnimalViewModel;
import com.azaryan.infrtestproject.presentation.presenter.list.ListPresenter;
import com.azaryan.infrtestproject.presentation.view.dogs.DogsView;

import java.util.List;

import io.reactivex.Single;

public class DogsPresenter extends ListPresenter<DogsView> {
    private final AnimalsInteractor interactor;

    public DogsPresenter(AnimalsInteractor interactor) {
        this.interactor = interactor;
    }

    @Override
    protected Single<List<AnimalViewModel>> getRequest() {
        return interactor.getDogs();
    }
}
