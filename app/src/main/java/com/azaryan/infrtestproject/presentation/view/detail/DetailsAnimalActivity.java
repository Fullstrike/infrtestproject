package com.azaryan.infrtestproject.presentation.view.detail;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import com.azaryan.infrtestproject.R;
import com.azaryan.infrtestproject.models.AnimalViewModel;
import com.squareup.picasso.Picasso;

public class DetailsAnimalActivity extends AppCompatActivity {
    private static final String EXTRA_DETAILS_DATA = "EXTRA_DETAILS_DATA";

    public static void start(Context context, AnimalViewModel viewModel) {
        Intent intent = new Intent(context, DetailsAnimalActivity.class);
        intent.putExtra(EXTRA_DETAILS_DATA, viewModel);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        Bundle extras = getIntent().getExtras();
        if (extras == null) {
            return;
        }

        AnimalViewModel viewModel = extras.getParcelable(EXTRA_DETAILS_DATA);
        if (viewModel == null) {
            return;
        }

        ((TextView) findViewById(R.id.tvTitle)).setText(viewModel.getTitle());
        ((TextView) findViewById(R.id.tvDescription)).setText(viewModel.getDescription());

        ImageView imageView = findViewById(R.id.ivIcon);
        Picasso.get().load(viewModel.getUrl()).into(imageView);
    }
}
