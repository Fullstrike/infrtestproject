package com.azaryan.infrtestproject.presentation.view.list;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.azaryan.infrtestproject.R;
import com.azaryan.infrtestproject.models.AnimalViewModel;
import com.squareup.picasso.Picasso;

import java.util.LinkedList;
import java.util.List;

public class AnimalsAdapter extends RecyclerView.Adapter<AnimalsAdapter.AnimalViewHolder> {

    private List<AnimalViewModel> items = new LinkedList<>();
    private OnItemClickListener listener;

    AnimalsAdapter(OnItemClickListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public AnimalViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_animal, viewGroup, false);

        return new AnimalViewHolder(view, listener);
    }

    @Override
    public void onBindViewHolder(@NonNull AnimalViewHolder viewHolder, int position) {
        viewHolder.bind(items.get(position));
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    void setItems(List<AnimalViewModel> items) {
        this.items.clear();
        this.items.addAll(items);
        notifyDataSetChanged();
    }

    class AnimalViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView tvTitle;
        private TextView tvDescription;
        private ImageView ivIcon;
        private AnimalViewModel viewModel;
        private OnItemClickListener listener;

        AnimalViewHolder(@NonNull View itemView, OnItemClickListener listener) {
            super(itemView);
            this.listener = listener;
            tvTitle = itemView.findViewById(R.id.tvTitle);
            tvDescription = itemView.findViewById(R.id.tvDescription);
            ivIcon = itemView.findViewById(R.id.ivIcon);
            itemView.setOnClickListener(this);
        }

        void bind(AnimalViewModel viewModel) {
            this.viewModel = viewModel;
            tvTitle.setText(viewModel.getTitle());
            tvDescription.setText(viewModel.getDescription());
            Picasso.get()
                    .load(viewModel.getUrl())
                    .into(ivIcon);
        }

        @Override
        public void onClick(View v) {
            listener.onItemClicked(viewModel);
        }
    }

    public interface OnItemClickListener {
        void onItemClicked(AnimalViewModel viewModel);
    }
}
