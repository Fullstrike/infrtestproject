package com.azaryan.infrtestproject.presentation.mvp;

import com.azaryan.infrtestproject.presentation.presenter.base.BasePresenter;

import java.util.HashMap;
import java.util.Map;

public class PresenterManager {
    private static PresenterManager manager;
    private Map<String, BasePresenter> presenterHolder = new HashMap<>();

    public static PresenterManager getInstance() {
        if (manager == null) {
            manager = new PresenterManager();
        }

        return manager;
    }

    public BasePresenter getPresenter(String tag) {
        if (!presenterHolder.containsKey(tag)) {
            return null;
        }

        return presenterHolder.get(tag);
    }

    public void removePresenter(String tag) {
        presenterHolder.remove(tag);
    }

    public void putPresenter(String tag, BasePresenter presenter) {
        presenterHolder.put(tag, presenter);
    }
}
