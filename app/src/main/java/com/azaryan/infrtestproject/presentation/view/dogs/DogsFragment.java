package com.azaryan.infrtestproject.presentation.view.dogs;

import com.azaryan.infrtestproject.DependencyProvider;
import com.azaryan.infrtestproject.presentation.presenter.base.BasePresenter;
import com.azaryan.infrtestproject.presentation.presenter.dogs.DogsPresenter;
import com.azaryan.infrtestproject.presentation.view.list.ListFragment;

public class DogsFragment extends ListFragment<DogsPresenter> implements DogsView {
    public static final String TAG = "DogsFragment";

    public static DogsFragment newInstance() {
        return new DogsFragment();
    }

    @Override
    protected BasePresenter createPresenter() {
        return new DogsPresenter(DependencyProvider.getInstance().provideInteractor());
    }
}
