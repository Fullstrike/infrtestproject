package com.azaryan.infrtestproject.models;

import java.util.ArrayList;
import java.util.List;

public class ListViewState {
    private List<AnimalViewModel> items = new ArrayList<>();
    private boolean isProgressVisible;

    public void showProgress() {
        isProgressVisible = true;
        items.clear();
    }

    public void hideProgress() {
        isProgressVisible = false;
    }

    public void showItems(List<AnimalViewModel> items) {
        hideProgress();
        this.items = items;
    }

    public List<AnimalViewModel> getItems() {
        return items;
    }

    public boolean isProgressVisible() {
        return isProgressVisible;
    }
}
