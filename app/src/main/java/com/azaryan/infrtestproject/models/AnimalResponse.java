package com.azaryan.infrtestproject.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AnimalResponse {
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private List<Animal> animals;

    public String getMessage() {
        return message;
    }

    public List<Animal> getAnimals() {
        return animals;
    }
}
