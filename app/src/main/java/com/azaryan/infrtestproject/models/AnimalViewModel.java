package com.azaryan.infrtestproject.models;

import android.os.Parcel;
import android.os.Parcelable;

public class AnimalViewModel implements Parcelable {
    private String title;
    private String description;
    private String url;

    public AnimalViewModel(String title, String description, String url) {
        this.title = title;
        this.description = description;
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    protected AnimalViewModel(Parcel in) {
        title = in.readString();
        description = in.readString();
        url = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(description);
        dest.writeString(url);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<AnimalViewModel> CREATOR = new Parcelable.Creator<AnimalViewModel>() {
        @Override
        public AnimalViewModel createFromParcel(Parcel in) {
            return new AnimalViewModel(in);
        }

        @Override
        public AnimalViewModel[] newArray(int size) {
            return new AnimalViewModel[size];
        }
    };
}
