package com.azaryan.infrtestproject.models;

import com.google.gson.annotations.SerializedName;

public class Animal {
    @SerializedName("url")
    private String url;
    @SerializedName("title")
    private String title;

    public String getUrl() {
        return url;
    }

    public String getTitle() {
        return title;
    }
}
